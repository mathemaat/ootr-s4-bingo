from enum import Enum, auto
import random
import sys

from util import DescriptiveEnum
from squares import Likelihood, squares, square_placement


def get_squares_grouped_by_likelihood():
    result = {}
    for likelihood in Likelihood:
        result[likelihood.name] = [s for s in squares if s.likelihood == likelihood]
    return result


class CardGenerator:

    def create_card(self):
        random.shuffle(squares)
        grouped_squares = get_squares_grouped_by_likelihood()
        selected_squares = []
        for placement in square_placement:
            selection = grouped_squares[placement.name].pop()
            selected_squares.append(selection)
        return selected_squares


class CardPrinter:

    DIMENSION = 5

    def __init__(self, card):
        self.card = card

    def print_card(self):
        raise NotImplementedError


class TextualCardPrinter(CardPrinter):

    CELL_WIDTH = 25
    CELL_HEIGHT = 9
    CELL_PADDING = 1

    def __init__(self, card):
        super().__init__(card=card)
        self.lines = []

    def print_card(self):
        # create empty card with grid lines
        standard_line = '|'.join([' ' * self.CELL_WIDTH for _ in range(self.DIMENSION)])
        divider_line = '+'.join(['-' * self.CELL_WIDTH for _ in range(self.DIMENSION)])
        for rowno in range(self.DIMENSION):
            if rowno >= 1:
                self.lines.append(divider_line)
            for _ in range(self.CELL_HEIGHT):
                self.lines.append(standard_line)

        # fill cell contents
        for i in range(self.DIMENSION ** 2):
            square = self.card[i]
            # calculate position of the square
            rowno, colno = i // self.DIMENSION, i % self.DIMENSION

            # place category
            self.place_text(rowno, colno, 1, square.category.value.upper())
            # place description
            parts = square.get_description_parts(self.CELL_WIDTH - 2 * self.CELL_PADDING)
            for i in range(len(parts)):
                self.place_text(rowno, colno, 3+i, parts[i])
            # place constraint
            parts = square.get_constraint_parts(self.CELL_WIDTH - 2 * self.CELL_PADDING)
            for i in range(len(parts)):
                self.place_text(rowno, colno, 5+i, parts[i])
            # place likelihood
            self.place_text(rowno, colno, 7, square.get_likelihood_text())
        
        for line in self.lines:
            print(line)
        print()

    def place_text(self, rowno, colno, lineno, text):
        # calulate indices of the top-left corner of the square
        x, y = rowno * (self.CELL_HEIGHT + 1) + lineno, colno * (self.CELL_WIDTH + 1)

        # center text
        num_spaces = (self.CELL_WIDTH - len(text)) // 2
        text = (num_spaces * ' ' + text).ljust(self.CELL_WIDTH)

        # place text in desired position
        self.lines[x] = self.lines[x][:y] + text + self.lines[x][y+len(text):]


class HtmlCardPrinter(CardPrinter):

    def print_card(self, name, index=0):
        table_html = self.generate_table_html()
        with open('outline.html') as f:
            html = f.read()
        html = html % table_html
        filename = 'cards/card-{}-{}.html'.format(name.lower(), index)
        with open(filename, 'w') as f:
            f.write(html)

    def generate_table_html(self):
        rows = []
        cells = []
        for i, square in enumerate(self.card):
            cells.append(square.to_html())
            if (i + 1) % self.DIMENSION == 0:
                row = "<tr>{}</tr>\n".format(''.join(cells))
                rows.append(row)
                cells = []
        html = "<table>\n{}</table>".format(''.join(rows))
        return html


def main(argv):
    try:
        name = argv[1]
        quantity = int(argv[2]) if len(argv) == 3 else 1
    except (IndexError, ValueError) as e:
        print('Specify name and quantity')
        sys.exit();
    creator = CardGenerator()
    for i in range(quantity):
        card = creator.create_card()
        printer = HtmlCardPrinter(card)
        printer.print_card(name=name, index=i)


if __name__ == '__main__':
    main(sys.argv)
