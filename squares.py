from util import DescriptiveEnum


class Likelihood(DescriptiveEnum):
    FREE = 'free'
    HIGH = 'high'
    MEDIUM = 'medium'
    LOW = 'low'


class CategoryType(DescriptiveEnum):
    GAMEPLAY_SPONGE = 'gameplay Sponge'
    GAMEPLAY_KARIOSSA = 'gameplay Kariossa'
    GAMEPLAY_EITHER = 'gameplay'
    QUOTE_CHAT = 'chat quote'
    QUOTE_COMMENTARY = 'commentary quote'
    QUOTE_ANY = 'commentary or chat quote'
    LOGIC = 'logic'
    HINTS = 'hints'
    ITEM_PLACEMENT = 'item placement'
    SPAWNS = 'spawns'


class Square:

    def __init__(self, likelihood, category, description, constraint):
        self.likelihood = likelihood
        self.category = category
        self.description = description
        self.constraint = constraint

    def __str__(self):
        return '{} ({})'.format(self.description, self.category.value)

    def __repr__(self):
        return '{}({}, {}, {}, {})'.format(
            self.__class__.__name__,
            self.likelihood,
            self.category,
            self.description,
            self.constraint,
        )

    def get_description_parts(self, max_width):
        return Square.get_parts(self.description, max_width)

    def get_constraint_parts(self, max_width):
        if self.constraint is None:
            return []
        return Square.get_parts(self.constraint, max_width)

    def get_likelihood_text(self):
        if self.likelihood == Likelihood.FREE:
            return self.likelihood.value
        return '{} likelihood'.format(self.likelihood.value)

    def to_html(self):
        html = "<p><b>{}</b></p>".format(self.category.value.upper())
        if self.description[0] == '"':
            html += "<p><i>{}</i></p>".format(self.description)
        elif self.description[0] == '{':
            endpos = self.description.find('}')
            html += "<p><i>{}</i> {}</p>".format(self.description[:endpos+1], self.description[endpos+2:])
        else:
            html += "<p>{}</p>".format(self.description)
        if self.constraint is not None:
            html += "<p>{}</p>".format(self.constraint)
        html = "<td style=\"background: #fff;\">{}</td>".format(html)
        return html

    @staticmethod
    def get_parts(text, max_width):
        # short descriptions don't need to be split up
        if len(text) <= max_width:
            return [text]

        # treat descriptions formatted as '{...} ...' as a special case
        if text[0] == '{':
            pos = text.find('}')
            first_line = text[:pos+1]
            second_line = text[pos+2:]
        else:
            words = text.split()
            first_line = words.pop(0)        
            while len(first_line) + len(words[0]) + 1 <= max_width:
                first_line += ' ' + words.pop(0)
            second_line = ' '.join(words)
        return [first_line, second_line]


squares = [
    Square(Likelihood.FREE, CategoryType.GAMEPLAY_SPONGE, 'Sponge going child', None),
    Square(Likelihood.FREE, CategoryType.GAMEPLAY_SPONGE, 'Sponge going adult', None),
    Square(Likelihood.FREE, CategoryType.GAMEPLAY_KARIOSSA, 'Kariossa going child', None),
    Square(Likelihood.FREE, CategoryType.GAMEPLAY_KARIOSSA, 'Kariossa going adult', None),
    Square(Likelihood.FREE, CategoryType.GAMEPLAY_EITHER, 'someone having ZL', None),
    Square(Likelihood.FREE, CategoryType.GAMEPLAY_EITHER, 'someone using LA on Ganondorf', None),
    Square(Likelihood.FREE, CategoryType.GAMEPLAY_EITHER, 'someone beats the game', 'within 4h'),
    Square(Likelihood.FREE, CategoryType.QUOTE_CHAT, '"rush ice"', None),
    Square(Likelihood.HIGH, CategoryType.QUOTE_CHAT, 'full right guess ("RRRRR")', 'shortly before lens game'),
    Square(Likelihood.HIGH, CategoryType.GAMEPLAY_EITHER, 'someone double-dipping GTG', None),
    Square(Likelihood.HIGH, CategoryType.GAMEPLAY_EITHER, 'someone buying a shield', None),
    Square(Likelihood.HIGH, CategoryType.GAMEPLAY_EITHER, 'someone setting FW', None),
    Square(Likelihood.HIGH, CategoryType.GAMEPLAY_EITHER, 'someone doing King Zora skip', None),
    Square(Likelihood.HIGH, CategoryType.GAMEPLAY_EITHER, 'someone doing Mido skip', None),
    Square(Likelihood.HIGH, CategoryType.GAMEPLAY_EITHER, 'someone being hit by keese', None),
    Square(Likelihood.HIGH, CategoryType.GAMEPLAY_EITHER, 'someone finding chus in a small chest', None),
    Square(Likelihood.HIGH, CategoryType.GAMEPLAY_SPONGE, '50 or lower on Dampé race', None),
    Square(Likelihood.HIGH, CategoryType.GAMEPLAY_KARIOSSA, '50 or lower on Dampé race', None),
    Square(Likelihood.HIGH, CategoryType.HINTS, 'nothing on skull mask', None),
    Square(Likelihood.HIGH, CategoryType.HINTS, 'nothing on Biggoron', None),
    Square(Likelihood.HIGH, CategoryType.HINTS, 'nothing on frogs 2', None),
    Square(Likelihood.HIGH, CategoryType.HINTS, 'nothing on 30 skulls', None),
    Square(Likelihood.HIGH, CategoryType.HINTS, 'nothing on 40 skulls', None),
    Square(Likelihood.HIGH, CategoryType.HINTS, 'nothing on 50 skulls', None),
    Square(Likelihood.HIGH, CategoryType.LOGIC, 'bomb-locked bomb bag', None),
    Square(Likelihood.HIGH, CategoryType.LOGIC, 'bow-locked bow', None),
    Square(Likelihood.HIGH, CategoryType.LOGIC, 'strength-locked strength', None),
    Square(Likelihood.MEDIUM, CategoryType.QUOTE_COMMENTARY, '"interesting seed"', None),
    Square(Likelihood.MEDIUM, CategoryType.QUOTE_COMMENTARY, '"Clean Barinade"', 'after beating Barinade'),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'diversion as early as minute one', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'one runner two medallions ahead', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'loser in Go mode when winner finishes', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'someone collecting more than 20 skulls', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'someone save-scumming keys', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'someone doing chickens', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'someone needing the light arrows hint', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'both runners in forest temple simultaneously', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'someone dipping BotW w/o bombs or ZL', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'skipping an important hint', 'intentionally or unintentionally'),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'skipping an important peek or check', 'intentionally or unintentionally'),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'exposing a scrub not using a nut', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'someone scoring 1650 or higher on HBA', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_EITHER, 'someone using bean flower transportation', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_SPONGE, 'chu swag in adult Spirit floormaster room', None),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_SPONGE, '0-cycle Phantom Ganon', 'kill w/o reflecting an energy ball'),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_KARIOSSA, 'doing river', 'within 30 minutes'),
    Square(Likelihood.MEDIUM, CategoryType.GAMEPLAY_KARIOSSA, 'entering ice before Sponge does', None),
    Square(Likelihood.MEDIUM, CategoryType.HINTS, 'path to King Dodongo, not for the bomb bag', None),
    Square(Likelihood.MEDIUM, CategoryType.HINTS, 'Kakariko is a path location', None),
    Square(Likelihood.MEDIUM, CategoryType.HINTS, 'song that locks checks on OoT', None),
    Square(Likelihood.MEDIUM, CategoryType.HINTS, 'warp song on OoT', None),
    Square(Likelihood.MEDIUM, CategoryType.HINTS, 'progesssion item on an always hint', None),
    Square(Likelihood.MEDIUM, CategoryType.HINTS, 'progesssion item on a sometimes hint', None),
    Square(Likelihood.MEDIUM, CategoryType.HINTS, 'a stone dungeon is path', None),
    Square(Likelihood.MEDIUM, CategoryType.HINTS, 'a medallion dungeon is foolish', None),
    Square(Likelihood.MEDIUM, CategoryType.HINTS, 'OGC, HC or HW foolish', None),
    Square(Likelihood.MEDIUM, CategoryType.ITEM_PLACEMENT, 'progesssion item on a rupee minigame', None),
    Square(Likelihood.MEDIUM, CategoryType.ITEM_PLACEMENT, 'progesssion item in a stone dungeon', None),
    Square(Likelihood.MEDIUM, CategoryType.ITEM_PLACEMENT, 'progesssion item on a boss', None),
    Square(Likelihood.MEDIUM, CategoryType.ITEM_PLACEMENT, 'water clearable without irons', None),
    Square(Likelihood.MEDIUM, CategoryType.LOGIC, 'two(+) paths for a single boss', None),
    Square(Likelihood.MEDIUM, CategoryType.LOGIC, 'three(+) paths converging to the same item', None),
    Square(Likelihood.MEDIUM, CategoryType.LOGIC, 'AD logically required', None),
    Square(Likelihood.MEDIUM, CategoryType.LOGIC, 'AD hard required', None),
    Square(Likelihood.MEDIUM, CategoryType.LOGIC, 'Pierre hard required', None),
    Square(Likelihood.MEDIUM, CategoryType.LOGIC, 'beans logically required', None),
    Square(Likelihood.MEDIUM, CategoryType.LOGIC, 'hookshot locked longshot', None),
    Square(Likelihood.MEDIUM, CategoryType.LOGIC, 'magic locked magic', None),
    Square(Likelihood.MEDIUM, CategoryType.SPAWNS, 'child overworld spawn', None),
    Square(Likelihood.MEDIUM, CategoryType.SPAWNS, 'adult overworld spawn', None),
    Square(Likelihood.MEDIUM, CategoryType.SPAWNS, 'Market spawn', 'child or adult'),
    Square(Likelihood.MEDIUM, CategoryType.SPAWNS, 'Kakariko spawn', 'child or adult'),
]

square_placement = [
    Likelihood.MEDIUM, Likelihood.HIGH, Likelihood.MEDIUM, Likelihood.HIGH, Likelihood.MEDIUM,
    Likelihood.HIGH, Likelihood.MEDIUM, Likelihood.MEDIUM, Likelihood.MEDIUM, Likelihood.HIGH,
    Likelihood.MEDIUM, Likelihood.MEDIUM, Likelihood.FREE, Likelihood.MEDIUM, Likelihood.MEDIUM,
    Likelihood.HIGH, Likelihood.MEDIUM, Likelihood.MEDIUM, Likelihood.MEDIUM, Likelihood.HIGH,
    Likelihood.MEDIUM, Likelihood.HIGH, Likelihood.MEDIUM, Likelihood.HIGH, Likelihood.MEDIUM,
]
